// 1.1 Basandote en el array siguiente, crea una lista ul > li dinámicamente en el html que imprima cada uno de los paises.

const listCountries = document.createElement('ul');
document.body.appendChild(listCountries);

const countries = ['Japón', 'Nicaragua', 'Suiza', 'Australia', 'Venezuela'];

countries.forEach(value => {
    const country = document.createElement('li');
    country.innerText = value;
    listCountries.appendChild(country);
});

// 1.2 Elimina el elemento que tenga la clase .fn-remove-me.

const remome = document.body.querySelector('.fn-remove-me');
document.body.removeChild(remome);

const listCars = document.createElement('ul');
document.body.appendChild(listCars);

// 1.3 Utiliza el array para crear dinamicamente una lista ul > li de elementos en el div de html con el atributo data-function="printHere".

const cars = ['Mazda 6', 'Ford fiesta', 'Audi A4', 'Toyota corola'];
cars.forEach(value => {
    const car = document.createElement('li');
    car.innerText = value;
    car.setAttribute('data-function','printHere');
    listCars.appendChild(car);
});

// 1.4 Crea dinamicamente en el html una lista de div que contenga un elemento  h4 para el titulo y otro elemento img para la imagen.


const list = [
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=1'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=2'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=3'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=4'},
	{title: 'Random title', imgUrl: 'https://picsum.photos/300/200?random=5'}
];

list.forEach((value, index) => {
    const title = document.createElement('h1');
    const img = document.createElement('img');

    title.innerText = value.title;
    img.setAttribute('src', value.imgUrl);

    document.body.appendChild(title);
    document.body.appendChild(img);
});

// 1.5 Basandote en el ejercicio anterior. Crea un botón que elimine el último elemento de la lista.

const myButton = document.createElement('button');
myButton.innerText = 'Eliminar elemento';
document.body.appendChild(myButton);

console.log(list);
myButton.addEventListener('click', () => {
    list.pop();
    console.log(list);
});